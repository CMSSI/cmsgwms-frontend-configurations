import os
import time

def findOverflowRegion(sites):
    if "_US_" in sites:
        OVERFLOW_US = "US"
    else:
        OVERFLOW_US = "none"

    if "_IT_" in sites:
        OVERFLOW_IT = "IT"
    else:
        OVERFLOW_IT = "none"

    if "_UK_London_" in sites:
        OVERFLOW_UK = "UK"
    else:
        OVERFLOW_UK = "none"

    return OVERFLOW_US + "," + OVERFLOW_IT + "," + OVERFLOW_UK

if __name__ == '__main__':
    print 'I am executing..'
