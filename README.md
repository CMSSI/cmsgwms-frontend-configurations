Repository for CMS GWMS frontend configurations management.

There are two main branches that correspond to respective frontends:
- CERN
- FNAL

The respective frontend configurations are placed in corresponding branches.
Each branch is further divided into directories based on pools.

The configuration contained in this repository is managed by [Kapitan](https://kapitan.dev/), master templates are found in the `templates` directory, the yaml-based input configuration is in `inventory` and the resulting configuration files are found in `compiled`. In order to re-generate the files in the `compiled` directory you can use the following command:

```
podman run -it -t --rm -u 0 -v $(pwd):/src:delegated registry.cern.ch/cmssi/kapitan kapitan compile && rmdir compiled/cern-* compiled/fnal-*
Add `--security-opt label=disable` to disable SELinux if you are running on an operating system that uses it.
```
