def match(job, glidein):
    return (
        # For the memory...
        (
            # GLIDEIN_MaxMemMBs was set to 0 on some "whole-node" entries
            (glidein["attrs"].get("GLIDEIN_MaxMemMBs", 0) == 0) or
            # For whole node pilots we don't know how much memory we will get. Assume it is more than the job request
            (glidein['attrs'].get('GLIDEIN_MaxMemMBs_Estimate', 'False') == 'True') or
            # Memory of the job less than memory of the partitionable slot in the pilot
            (job.get("RequestMemory", 99999) <= glidein["attrs"]["GLIDEIN_MaxMemMBs"])
        ) and 
        # For the OS...
        (
            # The job can run anywhere (OS is "any")
            (job.get("REQUIRED_OS", "any") == "any") or
            # If the pilot is any we can run
            (glidein["attrs"].get("GLIDEIN_REQUIRED_OS", "any")=="any") or
            # If nobosy sets any we check they are the same 
            (job.get("REQUIRED_OS") == glidein["attrs"]["GLIDEIN_REQUIRED_OS"])
        ) and
        # For the Architecture...
        (
            # The job can run anywhere (ARCH is "any")
            ("any" in job.get("REQUIRED_ARCH", "x86_64").split(",")) or
            # default in the entry actually means x86_64, matching all those jobs (and the ones without REQUIRED_ARCH)
            (glidein["attrs"].get("GLIDEIN_ARCH", "default") == "default" and "x86_64" in job.get("REQUIRED_ARCH", "x86_64").lower().split(",")) or
            # If the entry has an architecture that the job can use (using GLIDEIN_ARCH both for tarball selection and job matchmaking)
            (glidein["attrs"].get("GLIDEIN_ARCH") in job.get("REQUIRED_ARCH", "").lower().split(","))
        ) and
        # ...
        (
            # GLIDEIN_Job_Min_Time seems never defined in the factory. Not sure what it does...
            (job.get("MaxWallTimeMins", 0)*60) >= glidein["attrs"].get("GLIDEIN_Job_Min_Time", 0)
        ) and
        (
            # The walltime of the jobs has to be less than the walltime of the entry (minus 2 hours): wt < we - 2h
            (job.get("MaxWallTimeMins", 0)+10) < (glidein["attrs"]["GLIDEIN_Max_Walltime"]-glidein["attrs"]["GLIDEIN_Retire_Time_Spread"])/60
        )
    )

def match_site_or_gatekeeper(job, glidein):
    return (
            ((glidein["attrs"].get("GLIDEIN_CMSSite") in job["DESIRED_Sites"].split(",")) if ("DESIRED_Sites" in job) else False) or
            ((glidein["attrs"].get("GLIDEIN_Gatekeeper", "").split(" ")[0] in job["DESIRED_Gatekeepers"].split(",")) if ("DESIRED_Gatekeepers" in job) else False)
    )
# Function to match SiteName OR Entry OR Gatekeeper

def match_site_or_entry_or_gatekeeper(job, glidein):
    return (
        (
            match_site_or_gatekeeper(job, glidein) or
            ((glidein["attrs"].get("EntryName", "") in job["DESIRED_Entries"].split(",")) if ("DESIRED_Entries" in job) else False)
        )
    )

if __name__ == '__main__':
    pass
