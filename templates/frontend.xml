{%- set i = inventory.parameters -%}
{#- NOTE: 'Magic' default values were taken from the original template of the global pool, a more specific standard needs to be set-#}
<frontend advertise_delay="1" advertise_with_multiple="True" advertise_with_tcp="True" downtimes_file="frontenddowntime" frontend_monitor_index_page="True" frontend_name="{{ i.frontend_name }}" frontend_versioning="False" group_parallel_workers="2" loop_delay="120" restart_attempts="3" restart_interval="1800">
   <config>
      <idle_vms_total curb="{{ i.idle_vms_total.curb }}" max="{{ i.idle_vms_total.max }}"/>
      <idle_vms_total_global curb="{{ i.idle_vms_total_global.curb }}" max="{{ i.idle_vms_total_global.max }}"/>
      <running_glideins_total curb="{{ i.running_glideins_total.curb }}" max="{{ i.running_glideins_total.max }}"/>
      <running_glideins_total_global curb="{{ i.running_glideins_total_global.curb }}" max="{{ i.running_glideins_total_global.max }}"/>
   </config>
   <high_availability check_interval="300" enabled="{{ i.secondary }}">
      <ha_frontends>
         {% if i.ha_frontend_name is defined %}
         <ha_frontend frontend_name="{{ i.ha_frontend_name }}"/>
         {% endif %}
      </ha_frontends>
   </high_availability>
   <log_retention>
      <process_logs>
         <process_log backup_count="5" compression="" extension="info" max_days="7.0" max_mbytes="100.0" min_days="3.0" msg_types="INFO"/>
         <process_log backup_count="5" compression="" extension="err" max_days="7.0" max_mbytes="100.0" min_days="3.0" msg_types="ERR,WARN,EXCEPTION"/>
         <process_log backup_count="5" compression="" extension="debug" max_days="7.0" max_mbytes="100.0" min_days="3.0" msg_types="DEBUG"/>
      </process_logs>
   </log_retention>
   <match match_expr='__import__("CMSGlobalMatcher").match(job, glidein)' start_expr='ifthenelse(GLIDEIN_REQUIRED_OS=?="any", (HAS_SINGULARITY=?=true &amp;&amp; GLIDEIN_PS_HAS_SINGULARITY=!=false),( isUndefined(REQUIRED_OS) || REQUIRED_OS=?="any" || REQUIRED_OS=?=GLIDEIN_REQUIRED_OS )) &amp;&amp; ifthenelse(MaxWallTimeMins=!=UNDEFINED,(MaxWallTimeMins*60)&lt;(GLIDEIN_ToDie-MyCurrentTime),(16*3600)&lt;(GLIDEIN_ToDie-MyCurrentTime)) &amp;&amp; ((DynamicSlot =!= true) || (RequestCpus=?=Cpus)) &amp;&amp; ifthenelse(SlotType=?="Static", RequestCpus &lt;= Cpus, True) &amp;&amp; CMS_MATCH_MICROARCH'>
      <factory query_expr='{{ i.global_factory_expr }}'>
         <match_attrs>
            {% for attr in i.global_factory_expr_match_attrs %}
            <match_attr name="{{ attr.name }}" type="{{ attr.type | default('string') }}"/>
            {% endfor %}
         </match_attrs>
         <collectors>
            {% for collector in i.factory_collectors %}
            <collector DN="{{ collector.DN }}" factory_identity="{{ collector.factory_user }}@{{ collector.node }}" my_identity="{{ i.my_identity }}@{{ collector.node }}" node="{{ collector.node }}"/>
            {% endfor %}
         </collectors>
      </factory>
      <job query_expr='{{ i.job_query_expr | default('False') }}'>
         <match_attrs>
            {% for attr in i.job_query_expr_match_attrs %}
            <match_attr name="{{ attr.name }}" type="{{ attr.type | default('string') }}"/>
            {% endfor %}
         </match_attrs>
         <schedds>
            {% for schedd in i.schedds %}
            <schedd DN="{{ schedd.DN }}" fullname="{{ schedd.fullname }}"/>
            {% endfor %}
         </schedds>
      </job>
   </match>
   <monitor base_dir="/var/lib/gwms-frontend/web-area/monitor" flot_dir="/usr/share/javascriptrrd/flot" javascriptRRD_dir="/usr/share/javascriptrrd/js" jquery_dir="/usr/share/javascriptrrd/flot"/>
   <monitor_footer display_txt="" href_link=""/>
   <security classad_proxy="{{ i.classad_proxy }}" proxy_DN="{{ i.proxy_DN }}" proxy_selection_plugin="ProxyAll" security_name="{{ i.security_name }}" sym_key="aes_256_cbc">
      <credentials>
      </credentials>
   </security>
   <stage base_dir="/var/lib/gwms-frontend/web-area/stage" use_symlink="True" web_base_url="http://{{ i.frontend_fqdn }}/vofrontend/stage"/>
   <work base_dir="/var/lib/gwms-frontend/vofrontend" base_log_dir="{{ i.base_log_dir | default("/var/log/gwms-frontend") }}"/>
   <attrs>
      <attr name="CMS_GLIDEIN_VERSION" glidein_publish="True" job_publish="True" parameter="True" type="int" value="{{ i.cms_version }}"/>
      <attr name="CMSSF_ResourceType" glidein_publish="True" job_publish="True" parameter="True" type="string" value="x86_64:pledged"/>
      <attr name="CONDOR_VERSION" glidein_publish="False" job_publish="False" parameter="True" type="string" value="{{ i.condor_version }}"/>
      <attr name="CONDOR_OS" glidein_publish="False" job_publish="False" parameter="True" type="string" value="auto"/>
      <attr name="GLIDEIN_Expose_Grid_Env" glidein_publish="True" job_publish="True" parameter="False" type="string" value="True"/>
      <attr name="GLIDEIN_Job_Max_Time" comment="How long should a job continue to run when a glidein is in drain period" glidein_publish="True" job_publish="True" parameter="True" type="int" value="14400"/>
      <attr name="GLIDEIN_Monitoring_Enabled" comment="Monitoring slots are too expensive" glidein_publish="False" job_publish="False" parameter="True" type="string" value="False"/>
      <attr name="GLIDEIN_Report_Failed" glidein_publish="True" job_publish="False" parameter="False" type="string" value="ALWAYS"/>
      <!-- This is to clear BASH_FUNC_module and BASH_FUNC_spack, https://cdcvs.fnal.gov/redmine/issues/24227 -->
      <attr name="MODULE_USE" glidein_publish="True" job_publish="True" parameter="False" type="int" value="0"/>
      <!-- In the original singularity_validation.sh these cvmfs repos were manually opened. Known issue in RHEL6 bare metal hosts: repos needs to be mounted before the singularity image is loaded-->
      <attr name="CVMFS_REPOS_LIST" glidein_publish="True" job_publish="True" parameter="False" type="string" value="cms.cern.ch,oasis.opensciencegrid.org,singularity.opensciencegrid.org"/>
      <attr name="GLIDEIN_Singularity_Use" glidein_publish="True" job_publish="True" parameter="True" type="string" value="REQUIRED"/>
      <attr name="SINGULARITY_IMAGES_DICT" glidein_publish="True" job_publish="True" parameter="False" type="string" value="{{ i.singularity_images }}"/>
      <attr name="SLOTS_LAYOUT" glidein_publish="True" job_publish="True" parameter="False" type="string" value="partitionable"/>
      <attr name="UPDATE_INTERVAL" glidein_publish="True" job_publish="False" parameter="False" type="expr" value="$RANDOM_INTEGER(680,750,1)"/>
      <attr name="USE_MATCH_AUTH" glidein_publish="False" job_publish="False" parameter="True" type="string" value="True"/>
      <attr name="USE_PSS" glidein_publish="True" job_publish="True" parameter="True" type="expr" value="True"/>
      <attr name="STARTD_JOB_ATTRS" glidein_publish="True" job_publish="False" parameter="True" type="expr" value="$(STARTD_JOB_ATTRS),MemoryUsage,ResidentSetSize,ProportionalSetSizeKb"/>
      <attr name="STARTD_PARTITIONABLE_SLOT_ATTRS" glidein_publish="True" job_publish="False" parameter="False" type="expr" value="MemoryUsage,ProportionalSetSizeKb"/>
      <attr name="TotalMemoryUsage" glidein_publish="True" job_publish="False" parameter="False" type="expr" value="ifthenelse(sum(ChildMemoryUsage)=!=ERROR,sum(ChildMemoryUsage),0)"/>
      <attr name="MEMORY_USAGE_METRIC" glidein_publish="True" job_publish="True" parameter="True" type="expr" value="((ProportionalSetSizeKb ?: ResidentSetSize) + 1023) / 1024"/>
      {% for attr in i.extra_attrs|default([]) %}
      <attr name="{{ attr.name }}" glidein_publish="{{ attr.glidein_publish }}" job_publish="{{ attr.job_publish }}" parameter="{{ attr.parameter }}" type="{{ attr.type }}" value="{{ attr.value }}"/>
      {% endfor %}
   </attrs>
   <groups>
      {% for name, group in i.groups.items() %}
      <group name="{{ name }}" comment="{{ group.comment|default("No comment") }}" enabled="{{ group.enabled | default(false) }}">
         <config>
            {% if group.glideins_removal is defined %}
            <glideins_removal margin="{{ group.glideins_removal.margin }}" requests_tracking="{{ group.glideins_removal.requests_tracking }}" type="{{ group.glideins_removal.type }}" wait="{{ group.glideins_removal.wait }}"/>
            {% endif %}
            {% if group.idle_glideins_lifetime is defined %}
            <idle_glideins_lifetime max="{{ group.idle_glideins_lifetime.max }}"/>
            {% endif %}
            {% if group.idle_glideins_per_entry is defined %}
            <idle_glideins_per_entry max="{{ group.idle_glideins_per_entry.max }}" reserve="{{ group.idle_glideins_per_entry.reserve }}"/>
            {% endif %}
            {% if group.idle_vms_per_entry is defined %}
            <idle_vms_per_entry curb="{{ group.idle_vms_per_entry.curb }}" max="{{ group.idle_vms_per_entry.max }}"/>
            {% endif %}
            {% if group.idle_vms_total is defined %}
            <idle_vms_total curb="{{ group.idle_vms_total.curb }}" max="{{ group.idle_vms_total.max }}"/>
            {% endif %}
            {% if group.matchmakers is defined %}
            <processing_workers matchmakers="{{ group.matchmakers }}"/>
            {% endif %}
            {% if group.running_glideins_per_entry is defined %}
            <running_glideins_per_entry max="{{ group.running_glideins_per_entry.max }}" min="{{ group.running_glideins_per_entry.min }}" relative_to_queue="1.05"/>
            {% endif %}
            {% if group.glideins_removal is defined %}
            <running_glideins_total curb="{{ group.running_glideins_total.curb }}" max="{{ group.running_glideins_total.max }}"/>
            {% endif %}
         </config>
         <match match_expr='{{ group.match_expr | default('False')}}' start_expr='{{ group.start_expr | default('False') }}'>
            <factory query_expr='{{ group.factory_query_expr | default('False')}}'>
               <match_attrs>
                  {% for attr in group.factory_query_expr_match_attrs %}
                  <match_attr name="{{ attr.name }}" type="{{ attr.type | default('string') }}"/>
                  {% endfor %}
               </match_attrs>
               <collectors>
               </collectors>
            </factory>
            <job query_expr='{{ group.job_query_expr | default('False') }}'>
               <match_attrs>
                  {% for attr in group.job_query_expr_match_attrs %}
                  <match_attr name="{{ attr.name }}" type="{{ attr.type | default('string') }}"/>
                  {% endfor %}
               </match_attrs>
               <schedds>
               </schedds>
            </job>
         </match>
         <security idtoken_lifetime="{% if group.idtoken_lifetime is defined %}{{ group.idtoken_lifetime }}{% else %}{{ i.idtoken_lifetime }}{% endif %}">
            {% if group.credentials is defined %}
            <credentials>
            {% for cred in group.credentials %}
               <credential absfname="{{ cred.absfname }}" security_class="{{ cred.security_class }}" trust_domain="grid" type="{{ cred.type }}"/>
            {% endfor %}
            </credentials>
            {% endif %}
         </security>
         <attrs>
         {% for attr in group.attrs|default([]) %}
            <attr name="{{ attr.name }}" glidein_publish="{{ attr.glidein_publish|default('False') }}" job_publish="{{ attr.job_publish|default(False) }}" parameter="{{ attr.parameter|default(False) }}" type="{{ attr.type|default('string') }}" value="{{ attr.value }}"/>
         {% endfor %}
         </attrs>
         <files>
         {% for f in group.files|default([]) %}
            <file absfname="{{ f.absfname }}" after_entry="{{ f.after_entry }}" const="{{ f.const }}" executable="{{ f.executable }}" untar="{{ f.untar }}" wrapper="{{ f.wrapper }}">
               <untar_options cond_attr="TRUE"/>
            </file>
         {% endfor %}
         </files>
      </group>
      {% endfor %}
   </groups>
   <ccbs>
      {% for ccb in i.ccbs|default([]) %}
         <ccb DN="{{ ccb.DN }}" group="{{ ccb.group }}" node="{{ ccb.node }}"/>
      {% endfor %}
   </ccbs>
   <collectors>
      {% for collector in i.collectors %}
      <collector DN="{{ collector.DN }}" group="{{ collector.group }}" node="{{ collector.node }}" secondary="{{ collector.secondary | default(False) }}"/>
      {% endfor %}
   </collectors>
   <files>
      <file absfname="{{ i.validation_repo_path }}/advertise_site_classads.sh" after_entry="True" after_group="True" const="True" executable="True" period="0" prefix="GLIDEIN_PS_" untar="False" wrapper="False">
         <untar_options cond_attr="TRUE"/>
      </file>
      <file absfname="{{ i.validation_repo_path }}/check_microarch.sh" after_entry="True" after_group="True" const="True" executable="True" period="0" prefix="GLIDEIN_PS_" untar="False" wrapper="False">
         <untar_options cond_attr="TRUE"/>
      </file>
      <file absfname="{{ i.validation_repo_path }}/test_squid/test_squid.tgz" after_entry="True" after_group="True" const="True" executable="False" period="0" prefix="GLIDEIN_PS_" untar="True" wrapper="False">
         <untar_options absdir_outattr="CMS_TEST_SQUID" cond_attr="TRUE"/>
      </file>
      <file absfname="{{ i.validation_repo_path }}/test_squid/test_squid.sh" after_entry="True" after_group="True" const="True" executable="True" period="0" prefix="GLIDEIN_PS_" untar="False" wrapper="False">
         <untar_options cond_attr="TRUE"/>
      </file>
      <file absfname="{{ i.validation_repo_path }}/setjava.sh" after_entry="True" after_group="False" const="True" executable="False" period="0" prefix="GLIDEIN_PS_" untar="False" wrapper="True">
         <untar_options cond_attr="TRUE"/>
      </file>
      <file absfname="{{ i.validation_repo_path }}/set_home_cms.source" after_entry="True" after_group="True" const="True" executable="False" period="0" prefix="GLIDEIN_PS_" untar="False" wrapper="True">
         <untar_options cond_attr="TRUE"/>
      </file>
      <file absfname="{{ i.validation_repo_path }}/set_hold.sh" after_entry="True" after_group="False" const="True" executable="True" period="0" prefix="GLIDEIN_PS_" untar="False" wrapper="False">
         <untar_options cond_attr="TRUE"/>
      </file>
      <file absfname="{{ i.validation_repo_path }}/export_siteconf_info.py" after_entry="True" after_group="True" const="True" executable="True" period="0" prefix="GLIDEIN_PS_" untar="False" wrapper="False">
         <untar_options cond_attr="TRUE"/>
      </file>
      <file absfname="/var/lib/gwms-frontend/web-base/frontend/default_singularity_wrapper.sh" after_entry="True" after_group="True" const="True" executable="False" period="0" prefix="GLIDEIN_PS_" untar="False" wrapper="True">
         <untar_options cond_attr="TRUE"/>
      </file>
      <file absfname="{{ i.validation_repo_path }}/DIRACbenchmark.py" after_entry="True" after_group="True" const="True" executable="True" period="0" prefix="GLIDEIN_PS_" untar="False" wrapper="False">
         <untar_options cond_attr="TRUE"/>
      </file>
      {% for file in i.extra_files|default([]) %}
      <file absfname="{{ file.absfname }}" after_entry="{{ file.after_entry }}" after_group="{{ file.after_group }}" const="{{ file.const }}" executable="{{ file.executable }}" period="{{ file.period }}" prefix="{{ file.prefix }}" untar="{{ file.untar }}" wrapper="{{ file.wrapper }}">
         <untar_options cond_attr="TRUE"/>
      </file>
      {% endfor %}
   </files>
</frontend>
